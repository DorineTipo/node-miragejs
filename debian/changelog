node-miragejs (0.1.46+~cs5.6.11-1) unstable; urgency=medium

  * Team upload
  * New upstream version 0.1.46+~cs5.6.11

 -- Yadd <yadd@debian.org>  Fri, 25 Nov 2022 14:44:27 +0100

node-miragejs (0.1.45+~cs5.6.11-5) unstable; urgency=medium

  * Team upload
  * Replace /releases by /tags in GitHub urls

 -- Yadd <yadd@debian.org>  Mon, 31 Oct 2022 09:25:43 +0100

node-miragejs (0.1.45+~cs5.6.11-4) unstable; urgency=medium

  * Team upload
  * Add fix for rollup 3 (Closes: #1022631)

 -- Yadd <yadd@debian.org>  Wed, 26 Oct 2022 14:18:07 +0200

node-miragejs (0.1.45+~cs5.6.11-3) unstable; urgency=medium

  * Team upload
  * Fix autopkgtest

 -- Yadd <yadd@debian.org>  Fri, 23 Sep 2022 16:51:01 +0200

node-miragejs (0.1.45+~cs5.6.11-2) unstable; urgency=medium

  * Team upload
  * Add missing ${misc:Depends} to Depends for node-miragejs.
  * Update patches for recent @rollup/plugin-node-resolve

 -- Yadd <yadd@debian.org>  Fri, 23 Sep 2022 15:39:54 +0200

node-miragejs (0.1.45+~cs5.6.11-1) unstable; urgency=medium

  * Team upload
  * Declare compliance with policy 4.6.1
  * New upstream version 0.1.45+~cs5.6.11
  * Update patches

 -- Yadd <yadd@debian.org>  Mon, 29 Aug 2022 16:54:18 +0200

node-miragejs (0.1.42+~cs5.6.11-2) unstable; urgency=medium

  * Team upload
  * Build-require node-whatwg-fetch ≥ 3.6.2-5~ (fixes debci)

 -- Yadd <yadd@debian.org>  Mon, 15 Nov 2021 12:13:46 +0100

node-miragejs (0.1.42+~cs5.6.11-1) unstable; urgency=medium

  * Team upload

  [ lintian-brush ]
  * Set upstream metadata fields: Bug-Submit
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright)
  * Update standards version to 4.6.0, no changes needed

  [ Yadd ]
  * Add "Rules-Requires-Root: no"
  * Fix filenamemangle
  * Fix GitHub tags regex
  * Drop dependency to nodejs
  * Replace build dependency to libjs-fetch by node-whatwg-fetch
  * New upstream version 0.1.42+~cs5.6.11, updates:
    + miragejs 0.1.42
    + fake-xml-http-request 2.1.2
    + pretender 3.4.7
  * Update copyright
  * Install components in nodejs dir

 -- Yadd <yadd@debian.org>  Sun, 07 Nov 2021 09:22:01 +0100

node-miragejs (0.1.41+~cs5.6.6-4) unstable; urgency=medium

  * Drop legacy rollup-plugin-alias usage

 -- Pirate Praveen <praveen@debian.org>  Sat, 02 Jan 2021 23:48:38 +0530

node-miragejs (0.1.41+~cs5.6.6-3) unstable; urgency=medium

  * Add forwarded info to patch
  * Drop legacy rollup-plugin-babel
  * Replace deprecated CustomResolveOptions with moduleDirectories option
    (and update minimum version of node-rollup-plugin-node-resolve to 11)

 -- Pirate Praveen <praveen@debian.org>  Fri, 01 Jan 2021 17:56:47 +0530

node-miragejs (0.1.41+~cs5.6.6-2) unstable; urgency=medium

  * Drop legacy node-resolve and commonjs plugins

 -- Pirate Praveen <praveen@debian.org>  Wed, 30 Dec 2020 13:29:48 +0530

node-miragejs (0.1.41+~cs5.6.6-1) unstable; urgency=medium

  * Use checksum option in watch file
  * New upstream version 0.1.41+~cs5.6.6
  * Add comment to patch
  * Use dh-sequence-nodejs and simplify rules
  * Bump debhelper compatibility level to 13

 -- Pirate Praveen <praveen@debian.org>  Mon, 21 Dec 2020 16:15:03 +0530

node-miragejs (0.1.41-1) unstable; urgency=medium

  * Initial release (Closes: #977200)

 -- Pirate Praveen <praveen@debian.org>  Sat, 12 Dec 2020 18:41:02 +0530
